<?php

$plugin = array(
  'form' => 'feeds_tamper_embedded_media_form',
  'callback' => 'feeds_tamper_embedded_media_callback',
  'name' => 'Import embeded images',
  'multi' => 'skip',
  'category' => 'Other',
);

/**
 * Settings form.
 */
function feeds_tamper_embedded_media_form($importer, $element_key, $settings) {
  $form = array();

  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => t('This plugin expects to be given a full remote URL as a file to download.'),
  );
  $form['domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory domain'),
    '#default_value' => isset($settings['domain']) ? $settings['domain'] : '',
    '#description' => t('Ex: http://example.com'),
  );
  return $form;
}

/**
 * Fetcher callback.
 */
function feeds_tamper_embedded_media_callback($result, $item_key, $element_key, &$field, $settings, $source) {
  //@TODO Handle all media.
  //@TODO Log unsaved media.

  // Find all images.
  preg_match_all("/<img.*?src=\"(.*?)\".*?>/iS", $field, $matches);
  
  foreach ($matches[1] as $match) {
    $file_path = $match;
    if (!check_image_url($file_path)) {
      if (isset($settings['domain'])) {
        $file_path = $settings['domain'] . $file_path;
      }
    }
    try {
      $provider = media_internet_get_provider($file_path);
      $provider->validate();
      $file = $provider->save();

      if ($file) {
        $field = str_replace($match, file_create_url($file->uri), $field);
      }
    }
    catch (Exception $e) {
      continue;
    }
  }
}

function check_image_url($url) {
  if (!valid_url($url, TRUE) || !in_array(file_uri_scheme($url), variable_get('media_fromurl_supported_schemes', array('http', 'https', 'ftp', 'smb', 'ftps')))) {
    return FALSE;
  }
  return TRUE;
}