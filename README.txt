Description -- 
Feeds Tamper Embedded Media is a new plugin for Feeds Tamper. It allows you to import embedding media items within the body such as videos and images.

-- TODO -- 
- Use the HTML DOM Document.
- Convert the image tag to media object.
- Import Youtube videos.

-- INSTALLATION --
Installation is the usual, see http://drupal.org/node/70151 for further information.

-- Configuration --
- If you are familiar with feeds tamper you won't encounter an issues as it works as the other plugins. If you are using Feeds Tamper for the first time, refer to https://www.drupal.org/node/1246562.
- If your embedded images don't have absolute URL's then add the hosted domain in tamper configuration. 